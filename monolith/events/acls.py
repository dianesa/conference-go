from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_image(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    query = f"{city},{state}"
    url = f"https://api.pexels.com/v1/search?query={query}"
    resp = requests.get(url, headers=headers)
    image_url = resp.json()
    print(image_url)
    return image_url["photos"][0]["src"]["original"]


def get_coord(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&limit=5&appid={OPEN_WEATHER_API_KEY}"
    resp = requests.get(url)
    coord = resp.json()
    return {
        "lat": coord[0]["lat"],
        "lon": coord[0]["lon"],
    }


def get_weather(lat, lon):

    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    response = requests.get(url)
    weather_data = response.json()
    temperature = weather_data["main"]["temp"]
    description = weather_data["weather"][0]["description"]

    return {
        "temp": temperature,
        "description": description,
    }
